﻿using jezykiInt.Assemblers;
using jezykiInt.Data;
using jezykiInt.Models;
using jezykiInt.Models.View;
using jezykiInt.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jezykiInt.Services
{
    public class TicketService : ITicketService
    {
        private readonly jezykiIntContext _context;
        private readonly ITicketAssembler _ticketAssembler;
        public TicketService(jezykiIntContext context, ITicketAssembler ticketAssembler)
        {
            _ticketAssembler = ticketAssembler;
            _context = context;
        }

        public Guid CreateTicket(Ticket ticket)
        {
            ticket.DateCreated = DateTime.UtcNow;
            ticket.DateModified = DateTime.UtcNow;
            _context.Ticket.Add(ticket);
            _context.SaveChanges();
            return ticket.Id;
        }

        public void DeleteTicket(Guid id)
        {
            var ticket = _context.Ticket.Find(id);
            if (ticket != null)
            {
                _context.Remove(ticket);
                _context.SaveChanges();
            }
        }

        public PaginatedList<Ticket> GetAllTickets(string sortOrder, string currentFilter, string searchString, int? pageNumber,int pageSize)
        {
            var result = _context.Ticket.AsQueryable();
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                result = result.Where(x => x.Comment.Contains(searchString)
                                       || x.Email.Contains(searchString)
                                       || x.Text.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "type_desc":
                    result = result.OrderByDescending(x => x.Type);
                    break;
                case "type":
                    result = result.OrderBy(x => x.Type);
                    break;
                case "DateCreated":
                    result = result.OrderBy(x => x.DateCreated);
                    break;
                case "dateCreated_desc":
                    result = result.OrderByDescending(x => x.DateCreated);
                    break;
                case "DateModified":
                    result = result.OrderBy(x => x.DateModified);
                    break;
                case "dateModified_desc":
                    result = result.OrderByDescending(x => x.DateModified);
                    break;
                default:
                    result = result.OrderByDescending(s => s.DateCreated);
                    break;
            }
            var paginatedResult = PaginatedList<Ticket>.Create(result.Select(x => _ticketAssembler.GetListTicket(x)).AsNoTracking(), pageNumber ?? 1, pageSize);
            return paginatedResult;
        }
        public List<Ticket> GetLatestTickets(int amonut = 5)
        {
            var result = _context.Ticket.OrderByDescending(x => x.DateCreated).Select(x=>_ticketAssembler.GetListTicket(x));
            if (result.Count() > 0)
                return result.Take(amonut).ToList();
            else
                return result.ToList();
        }

        public Ticket GetTicketById(Guid id)
        {
            return _context.Ticket.Find(id);
        }

        public void UpdateTicket(Ticket ticket)
        {
            ticket.DateModified = DateTime.UtcNow;
            _context.Ticket.Update(ticket);
            _context.SaveChanges();
        }
    }
}
