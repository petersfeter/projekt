﻿using jezykiInt.Models;
using jezykiInt.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jezykiInt.Services.Interfaces
{
    public interface ITicketService
    {
        public PaginatedList<Ticket> GetAllTickets(string sortOrder, string currentFilter, string searchString, int? pageNumber, int pageSize);
        public List<Ticket> GetLatestTickets(int amonut);
        Ticket GetTicketById(Guid id);
        Guid CreateTicket(Ticket ticket);
        void UpdateTicket(Ticket ticket);
        void DeleteTicket(Guid id);
    }
}
