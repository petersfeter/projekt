﻿using jezykiInt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jezykiInt.Assemblers
{
    public interface ITicketAssembler
    {
        Ticket GetListTicket(Ticket ticket);
    }
}
