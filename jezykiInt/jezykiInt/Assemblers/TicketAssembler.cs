﻿using jezykiInt.Models;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jezykiInt.Assemblers
{
    public class TicketAssembler : ITicketAssembler
    {
        public Ticket GetListTicket(Ticket ticket)
        {
            if (ticket != null)
            {
                ticket.Comment = ticket.Comment.Length > 50 ? ticket.Comment.Substring(0, 50) + "..." : ticket.Comment;
                ticket.Text = ticket.Text.Length > 50 ? ticket.Text.Substring(0, 50) + "..." : ticket.Text;
                return ticket;
            }
            return ticket;
        }
    }
}
