﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jezykiInt.Models.View
{
    public class TicketHomeViewModel
    {
        public List<Ticket> Tickets { get; set; }
        public bool IsAdmin { get; set; }
    }
}
