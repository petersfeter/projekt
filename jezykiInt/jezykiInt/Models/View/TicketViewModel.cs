﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jezykiInt.Models.View
{
    public class TicketViewModel
    {
        public PaginatedList<Ticket> Tickets { get; set; }
        public bool IsAdmin { get; set; }
        public Ticket Ticket { get; set; }
    }
}
