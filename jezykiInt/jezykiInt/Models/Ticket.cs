﻿using jezykiInt.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace jezykiInt.Models
{
    public class Ticket
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Adres e-mail jest wymagany.")]
        [StringLength(100, ErrorMessage = "Adres e-mail nie może być dłuższy niż 100 znaków.")]
        [RegularExpression("^[a-zA-Z0-9_.-]{1,}[@]{1}[a-zA-Z0-9-_]+[.]{1}[a-zA-Z0-9-.]+", ErrorMessage = "Szablon: \"xx@xx.xx\" może zawierać litery cyfry i znaki: '.', '-' i '_'.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Treść jest wymagana.")]
        [StringLength(500, ErrorMessage = "Treść nie może być dłuższa niż 500 znaków.")]
        public string Text { get; set; }
        public TicketTypeEnum Type { get; set; }
        public bool IsSolved { get; set; }
        public string Comment { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
