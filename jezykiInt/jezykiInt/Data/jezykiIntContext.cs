﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using jezykiInt.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace jezykiInt.Data
{
    public class jezykiIntContext : IdentityDbContext<IdentityUser>
    {
        public jezykiIntContext (DbContextOptions<jezykiIntContext> options)
            : base(options)
        {
        }

        public DbSet<jezykiInt.Models.Ticket> Ticket { get; set; }
    }
}
