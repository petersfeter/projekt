﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using jezykiInt.Data;
using jezykiInt.Models;
using jezykiInt.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using jezykiInt.Models.View;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace jezykiInt.Controllers
{
    public class TicketsController : Controller
    {
        private readonly ITicketService _ticketService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        
        public TicketsController(ITicketService ticketService, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _ticketService = ticketService;
        }

        public IActionResult Index(string sortOrder, string currentFilter, string searchString, int? pageNumber, int pageSize = 15)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["TypeSortParam"] = sortOrder == "type" ? "type_desc" : "type";
            ViewData["DateCreatedSortParm"] = sortOrder == "DateCreated" ? "dateCreated_desc" : "DateCreated";
            ViewData["DateModifiedSortParm"] = sortOrder == "DateModified" ? "dateModified_desc" : "DateModified";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            var tickets = _ticketService.GetAllTickets(sortOrder, currentFilter, searchString, pageNumber, pageSize);
            var viewModel = new TicketViewModel
            {
                Tickets = tickets,
                IsAdmin = userId != null ? true : false,
            };
            return View(viewModel);
        }

        public IActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Ticket ticket = _ticketService.GetTicketById(id.Value);

            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);

            var viewModel = new TicketViewModel
            {
                Ticket = ticket,
                IsAdmin = userId != null ? true : false
            };

            return View(viewModel);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Email,Text,Type")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                Guid id = _ticketService.CreateTicket(ticket);
                return View("Created",id);
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        public IActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Ticket ticket = _ticketService.GetTicketById(id.Value);
            if (ticket == null)
            {
                return NotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Guid id, [Bind("Id,IsSolved,Comment")] Ticket ticket)
        {
            if (id != ticket.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _ticketService.UpdateTicket(ticket);
                }
                catch (Exception e)
                {
                    throw e;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        [Authorize]
        public IActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = _ticketService.GetTicketById(id.Value);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            _ticketService.DeleteTicket(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
