﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using jezykiInt.Data;
using Microsoft.AspNetCore.Identity;
using jezykiInt.Services.Interfaces;
using jezykiInt.Services;
using jezykiInt.Assemblers;
using Microsoft.CodeAnalysis.Options;
using Microsoft.AspNetCore.HttpOverrides;
using System.Net;

namespace jezykiInt
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<jezykiIntContext>()
                .AddDefaultTokenProviders();
            //services.AddDefaultIdentity<IdentityUser>().AddEntityFrameworkStores<jezykiIntContext>();
            services.AddDbContext<jezykiIntContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("jezykiIntContext")));
            services.AddRazorPages(options =>
            {
                options.Conventions.AuthorizeAreaPage("Identity", "/Account/Login");
                options.Conventions.AuthorizeAreaPage("Identity", "/Account/Logout");
            });
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Identity/Account/Login";
                options.LogoutPath = $"/Identity/Account/Logout";
            });
            services.AddHttpContextAccessor();
            services.AddScoped<ITicketService, TicketService>();
            services.AddTransient<ITicketAssembler, TicketAssembler>();
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.KnownProxies.Add(IPAddress.Parse("10.0.0.100"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
